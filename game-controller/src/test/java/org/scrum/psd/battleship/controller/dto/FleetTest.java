package org.scrum.psd.battleship.controller.dto;


import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FleetTest {

    @Test
    void addShip() {
        Fleet fleet = new Fleet();
        Ship firstShip = new Ship("Aircraft Carrier", 3, List.of(
                new Position(Letter.A, 1),
                new Position(Letter.A, 2),
                new Position(Letter.A, 3)
        ));
        Ship secondShip = new Ship("Aircraft Carrier 2", 3, List.of(
                new Position(Letter.A, 5),
                new Position(Letter.A, 6),
                new Position(Letter.A, 7)
        ));
        boolean resultFirst = fleet.addShip(firstShip);
        boolean resultSecond = fleet.addShip(secondShip);
        assertTrue(resultFirst);
        assertTrue(resultSecond);
    }

    @Test
    void addShipWhenIntercept() {
        Fleet fleet = new Fleet();
        Ship firstShip = new Ship("Aircraft Carrier", 3, List.of(
                new Position(Letter.A, 1),
                new Position(Letter.A, 2),
                new Position(Letter.A, 3)
        ));
        Ship secondShip = new Ship("Aircraft Carrier 2", 3, List.of(
                new Position(Letter.A, 2),
                new Position(Letter.B, 2),
                new Position(Letter.C, 2)
        ));
        boolean resultFirst = fleet.addShip(firstShip);
        boolean resultSecond = fleet.addShip(secondShip);
        assertTrue(resultFirst);
        assertFalse(resultSecond);
    }
}