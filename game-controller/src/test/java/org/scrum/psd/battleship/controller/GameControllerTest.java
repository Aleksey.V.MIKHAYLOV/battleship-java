package org.scrum.psd.battleship.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.function.Executable;
import org.scrum.psd.battleship.controller.dto.Fleet;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.Arrays;
import java.util.List;

public class GameControllerTest {
    @Test
    public void testCheckIsHitTrue() {
        Fleet fleet = GameController.initUserFleet();
        int counter = 0;

        for (Ship ship : fleet.getShips()) {
            Letter letter = Letter.values()[counter];

            for (int i = 0; i < ship.getSize(); i++) {
                ship.getPositions().add(new Position(letter, i));
            }

            counter++;
        }

        boolean result = GameController.checkIsHit(fleet, new Position(Letter.A, 1));

        Assert.assertTrue(result);
    }

    @Test
    public void testCheckIsHitFalse() {
        Fleet fleet = GameController.initUserFleet();
        int counter = 0;

        for (Ship ship : fleet.getShips()) {
            Letter letter = Letter.values()[counter];

            for (int i = 0; i < ship.getSize(); i++) {
                ship.getPositions().add(new Position(letter, i));
            }

            counter++;
        }

        boolean result = GameController.checkIsHit(fleet, new Position(Letter.H, 1));

        Assert.assertFalse(result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCheckIsHitPositstionIsNull() {
        GameController.checkIsHit(GameController.initUserFleet(), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCheckIsHitShipIsNull() {
        GameController.checkIsHit(null, new Position(Letter.H, 1));
    }

    @Test
    public void testIsShipValidFalse() {
        Ship ship = new Ship("TestShip", 3);
        boolean result = GameController.isShipValid(ship);

        Assert.assertFalse(result);
    }

    @Test
    public void testIsShipValidTrue() {
        List<Position> positions = Arrays.asList(new Position(Letter.A, 1), new Position(Letter.A, 1), new Position(Letter.A, 1));
        Ship ship = new Ship("TestShip", 3, positions);

        boolean result = GameController.isShipValid(ship);

        Assert.assertTrue(result);
    }

    @Test
    private void testParsePositionTrueVertical() {
        String position = "A2D";
        Assertions.assertEquals(GameController.parsePosition(position, 2),
                List.of(new Position(Letter.A, 2), new Position(Letter.B, 2))
        );
    }

    @Test
    private void testParsePositionTrueHorizontal() {
        String position = "A2R";
        Assertions.assertEquals(GameController.parsePosition(position, 2),
                List.of(new Position(Letter.A, 2), new Position(Letter.A, 3))
        );
    }

    @Test
    private void testParsePositionFalse() {
        String position = "A2G";
        Assertions.assertThrows(IllegalArgumentException.class, () -> GameController.parsePosition(position, 4));
    }
}
