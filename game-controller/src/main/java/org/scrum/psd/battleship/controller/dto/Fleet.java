package org.scrum.psd.battleship.controller.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Fleet {

    public List<Ship> getShips() {
        return ships;
    }

    private final List<Ship> ships;

    public Fleet() {
        this.ships = new ArrayList<>();
    }

    public Fleet(List<Ship> ships) {
        this.ships = new ArrayList<>(ships);
    }

    public boolean addShip(Ship ship) {
        if (!hasIntercepts(ship)) {
            this.ships.add(ship);
            return true;
        }
        return false;
    }

    private boolean hasIntercepts(Ship ship) {
        List<Position> positions = ships.stream().flatMap(s -> s.getPositions().stream()).collect(Collectors.toList());
        for (Position position : ship.getPositions()) {
            if (positions.contains(position)) {
                return true;
            }
        }
        return false;
    }
}
