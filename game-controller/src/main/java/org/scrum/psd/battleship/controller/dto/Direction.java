package org.scrum.psd.battleship.controller.dto;

public enum Direction {
    U,
    D,
    L,
    R
}
