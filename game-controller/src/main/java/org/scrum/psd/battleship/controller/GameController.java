package org.scrum.psd.battleship.controller;

import org.scrum.psd.battleship.controller.dto.Color;
import org.scrum.psd.battleship.controller.dto.Direction;
import org.scrum.psd.battleship.controller.dto.Fleet;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import static org.scrum.psd.battleship.controller.dto.Direction.U;

public class GameController {
    public static boolean checkIsHit(Fleet feet, Position shot) {
        if (feet == null) {
            throw new IllegalArgumentException("feet is null");
        }

        if (shot == null) {
            throw new IllegalArgumentException("shot is null");
        }

        for (Ship ship : feet.getShips()) {
            for (Position position : ship.getPositions()) {
                if (position.equals(shot)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static List<Ship> initializeShips() {
        return Arrays.asList(
                new Ship("Aircraft Carrier", 5, Color.CADET_BLUE),
                new Ship("Battleship", 4, Color.RED),
                new Ship("Submarine", 3, Color.CHARTREUSE),
                new Ship("Destroyer", 3, Color.YELLOW),
                new Ship("Patrol Boat", 2, Color.ORANGE)
        );
    }

    public static Fleet initUserFleet() {
        return new Fleet(initializeShips());
    }

    public static boolean isShipValid(Ship ship) {
        return ship.getPositions().size() == ship.getSize();
    }

    public static Position getRandomPosition(int size) {
        Random random = new Random();
        Letter letter = Letter.values()[random.nextInt(size)];
        int number = random.nextInt(size);
        Position position = new Position(letter, number);
        return position;
    }

    protected static List<Position> parsePosition(String input, Integer size) {
        Letter letter;
        int number;
        Direction direction;

        try {
            letter = Letter.valueOf(input.toUpperCase().substring(0, 1));
            number = Integer.parseInt(input.substring(1, 2));
            direction = Direction.valueOf(input.substring(2));
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Sorry, value is incorrect. Please enter right coordinate");
        }

        List<Position> result = List.of();

        switch (direction) {
            case U: {
                for (int i = 0; i < size; i++) {
                    result.add(new Position(letter, number--));
                }
                break;
            }
            case D: {
                for (int i = 0; i < size; i++) {
                    result.add(new Position(letter, number++));
                }
                break;
            }
            case L: {
                result.add(new Position(letter, number));
                for (int i = 1; i < size; i++) {
                    letter = letter.next();
                    result.add(new Position(letter, number));
                }
                break;
            }
            case R: {
                result.add(new Position(letter, number));
                for (int i = 1; i < size; i++) {
                    letter = letter.prev();
                    result.add(new Position(letter, number));
                }
                break;
            }
        }

        return result;
    }
}
