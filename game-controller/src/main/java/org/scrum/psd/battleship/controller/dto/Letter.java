package org.scrum.psd.battleship.controller.dto;

public enum Letter {
    A, B, C, D, E, F, G, H;

    public Letter next() {
        int ordinal = ordinal();
        return values()[++ordinal];
    }

    public Letter prev() {
        int ordinal = ordinal();
        return values()[--ordinal];
    }

}

