package org.scrum.psd.battleship.ascii;

import com.diogonunes.jcdp.color.ColoredPrinter;
import com.diogonunes.jcdp.color.api.Ansi;

import java.util.Objects;

import static com.diogonunes.jcdp.Constants.NEWLINE;
import static com.diogonunes.jcdp.color.api.Ansi.Attribute.LIGHT;
import static com.diogonunes.jcdp.color.api.Ansi.POSTFIX;
import static com.diogonunes.jcdp.color.api.Ansi.SEPARATOR;

public class CustomizedColoredPrinter extends ColoredPrinter {

    public CustomizedColoredPrinter() {
        super.setAttribute(LIGHT);
        super.setForegroundColor(Ansi.FColor.WHITE);
        super.setBackgroundColor(Ansi.BColor.BLACK);
    }

    @Override
    public void print(Object msg) {
        String attr = generateCode(getAttribute(), getForegroundColor(), getBackgroundColor());
        formattedPrint(msg, attr, false);
    }

    @Override
    public void println(Object msg) {
        String paddedMsg = String.format("%-100s", msg);
        String attr = generateCode(getAttribute(), getForegroundColor(), getBackgroundColor());
        formattedPrint(paddedMsg, attr, true);
    }

    private void formattedPrint(Object msg, String ansiFormatCode, boolean appendNewline) {
        StringBuilder output = new StringBuilder();
        output.append(isLoggingTimestamps() ? getDateFormatted() + " " : "");
        output.append(msg);
        output.append(appendNewline ? NEWLINE : "");
        String formattedMsg = Ansi.formatMessage(output.toString(), ansiFormatCode);
        System.out.print(formattedMsg);
    }

    public static String generateCode(Object... options) {
        StringBuilder builder = new StringBuilder();

        builder.append(Ansi.PREFIX);
        for (Object option : options) {
            String code;
            if (option instanceof Ansi.FColor && Objects.equals(option, Ansi.FColor.WHITE)) {
                code = "97"; // bright white
            } else {
                code = option.toString();
            }
            if (code.equals(""))
                continue;
            builder.append(code);
            builder.append(SEPARATOR);
        }
        builder.append(POSTFIX);

        // because code must not end with SEPARATOR
        return builder.toString().replace(SEPARATOR + POSTFIX, POSTFIX);
    }

    @Override
    public void setForegroundColor(Ansi.FColor c) {
        super.setForegroundColor(c);
    }

    @Override
    public void setBackgroundColor(Ansi.BColor c) {
        super.setBackgroundColor(c);
    }

    @Override
    public void errorPrint(Object msg) {

    }

    @Override
    public void errorPrintln(Object msg) {

    }

    @Override
    public void debugPrint(Object msg) {

    }

    @Override
    public void debugPrint(Object msg, int level) {

    }

    @Override
    public void debugPrintln(Object msg) {

    }

    @Override
    public void debugPrintln(Object msg, int level) {

    }

    @Override
    public void print(Object msg, Ansi.Attribute attr, Ansi.FColor fg, Ansi.BColor bg) {

    }

    @Override
    public void println(Object msg, Ansi.Attribute attr, Ansi.FColor fg, Ansi.BColor bg) {

    }

    @Override
    public void errorPrint(Object msg, Ansi.Attribute attr, Ansi.FColor fg, Ansi.BColor bg) {

    }

    @Override
    public void errorPrintln(Object msg, Ansi.Attribute attr, Ansi.FColor fg, Ansi.BColor bg) {

    }

    @Override
    public void debugPrint(Object msg, Ansi.Attribute attr, Ansi.FColor fg, Ansi.BColor bg) {

    }

    @Override
    public void debugPrint(Object msg, int level, Ansi.Attribute attr, Ansi.FColor fg, Ansi.BColor bg) {

    }

    @Override
    public void debugPrintln(Object msg, Ansi.Attribute attr, Ansi.FColor fg, Ansi.BColor bg) {

    }

    @Override
    public void debugPrintln(Object msg, int level, Ansi.Attribute attr, Ansi.FColor fg, Ansi.BColor bg) {

    }
}
