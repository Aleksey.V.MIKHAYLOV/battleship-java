package org.scrum.psd.battleship.ascii;

import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.Arrays;
import java.util.List;

public class FleetVariants {
    public static List<Ship> initial = initialEnemyFleet();
    public static List<Ship> v1 = enemyFleetV1();
    public static List<Ship> v2 = enemyFleetV2();
    public static List<Ship> v3 = enemyFleetV3();
    public static List<Ship> v4 = enemyFleetV4();
    public static List<Ship> v5 = enemyFleetV5();

    public static List<List<Ship>> variants = Arrays.asList(v1, v2, v3, v4, v5);

    private static List<Ship> initialEnemyFleet() {
        List<Ship> enemyFleet = GameController.initializeShips();

        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 5));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 7));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 8));

        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 5));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 6));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 7));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 8));

        enemyFleet.get(2).getPositions().add(new Position(Letter.A, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.B, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.C, 3));

        enemyFleet.get(3).getPositions().add(new Position(Letter.F, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.G, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.H, 8));

        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 5));
        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 6));
        return enemyFleet;
    }

    private static List<Ship> enemyFleetV1() {
        List<Ship> enemyFleet = GameController.initializeShips();

        enemyFleet.get(0).getPositions().add(new Position(Letter.A, 1));
        enemyFleet.get(0).getPositions().add(new Position(Letter.B, 1));
        enemyFleet.get(0).getPositions().add(new Position(Letter.C, 1));
        enemyFleet.get(0).getPositions().add(new Position(Letter.D, 1));
        enemyFleet.get(0).getPositions().add(new Position(Letter.E, 1));

        enemyFleet.get(1).getPositions().add(new Position(Letter.B, 4));
        enemyFleet.get(1).getPositions().add(new Position(Letter.C, 4));
        enemyFleet.get(1).getPositions().add(new Position(Letter.D, 4));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 4));

        enemyFleet.get(2).getPositions().add(new Position(Letter.B, 7));
        enemyFleet.get(2).getPositions().add(new Position(Letter.C, 7));
        enemyFleet.get(2).getPositions().add(new Position(Letter.D, 7));

        enemyFleet.get(3).getPositions().add(new Position(Letter.G, 5));
        enemyFleet.get(3).getPositions().add(new Position(Letter.G, 6));
        enemyFleet.get(3).getPositions().add(new Position(Letter.G, 7));

        enemyFleet.get(4).getPositions().add(new Position(Letter.G, 2));
        enemyFleet.get(4).getPositions().add(new Position(Letter.H, 2));
        return enemyFleet;
    }

    private static List<Ship> enemyFleetV2() {
        List<Ship> enemyFleet = GameController.initializeShips();

        enemyFleet.get(0).getPositions().add(new Position(Letter.H, 1));
        enemyFleet.get(0).getPositions().add(new Position(Letter.H, 2));
        enemyFleet.get(0).getPositions().add(new Position(Letter.H, 3));
        enemyFleet.get(0).getPositions().add(new Position(Letter.H, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.H, 5));

        enemyFleet.get(1).getPositions().add(new Position(Letter.B, 4));
        enemyFleet.get(1).getPositions().add(new Position(Letter.B, 4));
        enemyFleet.get(1).getPositions().add(new Position(Letter.D, 4));
        enemyFleet.get(1).getPositions().add(new Position(Letter.E, 4));

        enemyFleet.get(2).getPositions().add(new Position(Letter.B, 6));
        enemyFleet.get(2).getPositions().add(new Position(Letter.C, 6));
        enemyFleet.get(2).getPositions().add(new Position(Letter.D, 6));

        enemyFleet.get(3).getPositions().add(new Position(Letter.B, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.C, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.D, 8));

        enemyFleet.get(4).getPositions().add(new Position(Letter.G, 2));
        enemyFleet.get(4).getPositions().add(new Position(Letter.H, 2));
        return enemyFleet;
    }

    private static List<Ship> enemyFleetV3() {
        List<Ship> enemyFleet = GameController.initializeShips();

        enemyFleet.get(0).getPositions().add(new Position(Letter.A, 1));
        enemyFleet.get(0).getPositions().add(new Position(Letter.A, 2));
        enemyFleet.get(0).getPositions().add(new Position(Letter.A, 3));
        enemyFleet.get(0).getPositions().add(new Position(Letter.A, 4));
        enemyFleet.get(0).getPositions().add(new Position(Letter.A, 5));

        enemyFleet.get(1).getPositions().add(new Position(Letter.D, 3));
        enemyFleet.get(1).getPositions().add(new Position(Letter.D, 4));
        enemyFleet.get(1).getPositions().add(new Position(Letter.D, 5));
        enemyFleet.get(1).getPositions().add(new Position(Letter.D, 6));

        enemyFleet.get(2).getPositions().add(new Position(Letter.F, 4));
        enemyFleet.get(2).getPositions().add(new Position(Letter.G, 4));
        enemyFleet.get(2).getPositions().add(new Position(Letter.H, 4));

        enemyFleet.get(3).getPositions().add(new Position(Letter.F, 6));
        enemyFleet.get(3).getPositions().add(new Position(Letter.G, 6));
        enemyFleet.get(3).getPositions().add(new Position(Letter.H, 6));

        enemyFleet.get(4).getPositions().add(new Position(Letter.G, 8));
        enemyFleet.get(4).getPositions().add(new Position(Letter.H, 8));
        return enemyFleet;
    }

    private static List<Ship> enemyFleetV4() {
        List<Ship> enemyFleet = GameController.initializeShips();

        enemyFleet.get(0).getPositions().add(new Position(Letter.C, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.D, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.E, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.F, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.G, 6));

        enemyFleet.get(1).getPositions().add(new Position(Letter.D, 1));
        enemyFleet.get(1).getPositions().add(new Position(Letter.D, 2));
        enemyFleet.get(1).getPositions().add(new Position(Letter.D, 3));
        enemyFleet.get(1).getPositions().add(new Position(Letter.D, 4));

        enemyFleet.get(2).getPositions().add(new Position(Letter.G, 2));
        enemyFleet.get(2).getPositions().add(new Position(Letter.G, 3));
        enemyFleet.get(2).getPositions().add(new Position(Letter.G, 4));

        enemyFleet.get(3).getPositions().add(new Position(Letter.E, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.F, 8));
        enemyFleet.get(3).getPositions().add(new Position(Letter.G, 8));

        enemyFleet.get(4).getPositions().add(new Position(Letter.B, 8));
        enemyFleet.get(4).getPositions().add(new Position(Letter.C, 8));
        return enemyFleet;
    }

    private static List<Ship> enemyFleetV5() {
        List<Ship> enemyFleet = GameController.initializeShips();

        enemyFleet.get(0).getPositions().add(new Position(Letter.D, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.E, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.F, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.G, 6));
        enemyFleet.get(0).getPositions().add(new Position(Letter.H, 6));

        enemyFleet.get(1).getPositions().add(new Position(Letter.B, 2));
        enemyFleet.get(1).getPositions().add(new Position(Letter.B, 3));
        enemyFleet.get(1).getPositions().add(new Position(Letter.B, 4));
        enemyFleet.get(1).getPositions().add(new Position(Letter.B, 5));

        enemyFleet.get(2).getPositions().add(new Position(Letter.E, 2));
        enemyFleet.get(2).getPositions().add(new Position(Letter.F, 2));
        enemyFleet.get(2).getPositions().add(new Position(Letter.G, 2));

        enemyFleet.get(3).getPositions().add(new Position(Letter.D, 4));
        enemyFleet.get(3).getPositions().add(new Position(Letter.E, 4));
        enemyFleet.get(3).getPositions().add(new Position(Letter.F, 4));

        enemyFleet.get(4).getPositions().add(new Position(Letter.A, 8));
        enemyFleet.get(4).getPositions().add(new Position(Letter.B, 8));
        return enemyFleet;
    }
}
