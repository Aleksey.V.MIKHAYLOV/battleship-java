package org.scrum.psd.battleship.ascii;

import com.diogonunes.jcdp.color.api.Ansi;
import org.scrum.psd.battleship.controller.GameController;
import org.scrum.psd.battleship.controller.dto.Fleet;
import org.scrum.psd.battleship.controller.dto.Letter;
import org.scrum.psd.battleship.controller.dto.Position;
import org.scrum.psd.battleship.controller.dto.Ship;

import java.util.Random;
import java.util.Scanner;

public class Main {
    private static Fleet myFleet;
    private static Fleet enemyFleet;
    private static CustomizedColoredPrinter console;

    private static Ansi.FColor baseColor = Ansi.FColor.WHITE;
    private static Ansi.FColor imagesColor = Ansi.FColor.MAGENTA;
    private static Ansi.FColor systemColor = Ansi.FColor.YELLOW;
    private static Ansi.FColor delimeterColor = Ansi.FColor.GREEN;
    private static Ansi.FColor missColor = Ansi.FColor.BLUE;
    private static Ansi.FColor hitColor = Ansi.FColor.RED;

    public static void main(String[] args) {
        console = new CustomizedColoredPrinter();

        console.setForegroundColor(imagesColor);
        console.println("                                          |__");
        console.println("                                          |\\/");
        console.println("                                          ---");
        console.println("                                          / | [");
        console.println("                                   !      | |||");
        console.println("                                 _/|     _/|-++'");
        console.println("                             +  +--|    |--|--|_ |-");
        console.println("                          { /|__|  |/\\__|  |--- |||__/");
        console.println("                         +---------------___[}-_===_.'____                 /\\");
        console.println("                     ____`-' ||___-{]_| _[}-  |     |_[___\\==--            \\/   _");
        console.println("      __..._____--==/___]_|__|_____________________________[___\\==--____,------' .7");
        console.println("     |                        Welcome to Battleship                         BB-61/");
        console.println("      \\_________________________________________________________________________|");
        console.println("     ");
        console.setForegroundColor(baseColor);

        InitializeGame();

        StartGame();
    }

    private static void StartGame() {
        Scanner scanner = new Scanner(System.in);

        console.setForegroundColor(imagesColor);
        console.print("\033[2J\033[;H");
        console.println("                  __");
        console.println("                 /  \\");
        console.println("           .-.  |    |");
        console.println("   *    _.-'  \\  \\__/");
        console.println("    \\.-'       \\");
        console.println("   /          _/");
        console.println("  |      _  /\" \"");
        console.println("  |     /_\'");
        console.println("   \\    \\_/");
        console.println("    \" \"\" \"\" \"\" \"");
        console.setForegroundColor(baseColor);

        do {
            printDelimeter();
            console.setForegroundColor(baseColor);
            console.println("Player, it's your turn!");
            console.setForegroundColor(systemColor);
            console.println("Enter coordinates for your shot :");

            Position position = getRightPosition(scanner);

            boolean isHit = GameController.checkIsHit(enemyFleet, position);

            if (isHit) {
                beep();
                printHit();
                drawHit();
            } else {
                console.setForegroundColor(missColor);
                console.println("Miss");
            }

            console.println(" ");
            console.setForegroundColor(baseColor);
            console.println("Computer turn!");

            position = getRandomPosition();
            isHit = GameController.checkIsHit(myFleet, position);

            if (isHit) {
                beep();
                console.setForegroundColor(hitColor);
                console.println(String.format("Computer shoot in %s%s and %s", position.getColumn(), position.getRow(), "hit your ship !"));
                drawHit();
            } else {
                console.setForegroundColor(missColor);
                console.println(String.format("Computer shoot in %s%s and %s", position.getColumn(), position.getRow(), "miss"));
            }

            // TODO: завершение игры
        } while (true);
    }

    private static void beep() {
        console.print("\007");
    }



    private static Position getRandomPosition() {
        int rows = 8;
        int lines = 8;
        Random random = new Random();
        Letter letter = Letter.values()[random.nextInt(lines)];
        int number = random.nextInt(rows);
        Position position = new Position(letter, number);
        return position;
    }

    private static void InitializeGame() {
        InitializeMyFleet();

        InitializeEnemyFleet();
    }

    private static void InitializeMyFleet() {
        Scanner scanner = new Scanner(System.in);
        myFleet = GameController.initUserFleet();

        console.println(" ");
        console.println(" ");
        console.setForegroundColor(baseColor);
        console.println("Please position your fleet (Game board has size from A to H and 1 to 8) :");

        for (Ship ship : myFleet.getShips()) {
            console.println(" ");
            console.setForegroundColor(baseColor);
            console.println(String.format("Please enter the first position and direction %s (size: %s)", ship.getName(), ship.getSize()));


            console.setForegroundColor(systemColor);
            console.println("Enter first position");

            Position position = getRightPosition(scanner);


            for (int i = 1; i <= ship.getSize(); i++) {
                console.setForegroundColor(systemColor);
                console.println(String.format("Enter position %s of %s (i.e A3):", i, ship.getSize()));

                Position position = getRightPosition(scanner);
                ship.addPosition(position);
            }
            break;
        }
        console.println(" ");
        console.println(" ");
    }

    private static void printDelimeter() {
        console.setForegroundColor(delimeterColor);
        console.println("   ");
        console.println("=========================================");
    }

    private static void printHit() {
        console.setForegroundColor(hitColor);
        console.println("Yeah ! Nice hit !");
    }
    private static void drawHit() {

        console.setForegroundColor(hitColor);
        console.println("     _.-^^---....,,--       ");
        console.println(" _--                  --_  ");
        console.println("<                        >)");
        console.println("|                         | ");
        console.println(" \\._                   _./  ");
        console.println("    ```--. . , ; .--'''       ");
        console.println("          | |   |             ");
        console.println("       .-=||  | |=-.   ");
        console.println("       `-=#$%&%$#=-'   ");
        console.println("          | ;  :|     ");
        console.println(" _____.,-#%&$@%#&#~,._____");
    }

    private static void InitializeEnemyFleet() {
        enemyFleet = new Fleet(FleetVariants.variants.get(new Random().nextInt(5)));
    }


}
